package de.gjw.gjwfreizeitanmeldung.ui.freizeit;

import android.app.ProgressDialog;
import android.util.Log;
import android.util.Xml;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import org.xmlpull.v1.XmlPullParser;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import de.gjw.gjwfreizeitanmeldung.MainActivity;
import de.gjw.gjwfreizeitanmeldung.ui.Members.EditFragment;
import de.gjw.gjwfreizeitanmeldung.ui.Members.Member;
import de.gjw.gjwfreizeitanmeldung.ui.settings.SettingsFragment;

public class Freizeit {

    public static Freizeit KeineFreizeiten=new Freizeit();
    private static ArrayList<Freizeit>[] Freizeiten;

    public static String info_GJW_lang;
    public static String info_GJW_Straße;
    public static String info_GJW_Plz;
    public static String info_GJW_Ort;
    public static String info_GJW_Telefon;
    public static String info_GJW_Telefax;
    public static String info_GJW_Email;
    public static String info_GJW_Website;

    public static String[] werkeListen=new String[]
            {
                    "https://gjw.events/gjw-deutschland/",
                    "https://gjw.events/gjw-bawue/",
                    "https://gjw.events/gjw-bayern/",
                    "https://gjw.events/gjw-bb/",
                    "https://gjw.events/gjw-hessen/",
                    "https://gjw.events/gjw-mv/",
                    "https://gjw.events/gjw-nd/",
                    "https://gjw.events/gjw-nos/",
                    "https://gjw.events/gjw-nrw/",
                    "https://gjw.events/gjw-nwd/",
                    "https://gjw.events/gjw-sachsen/",
                    "https://gjw.events/gjw-th/"
            };

    int werkID;
    String id;
    String place;
    int minAge;
    int maxAge;
    String begin;
    String end;
    String price;
    String title;
    public static void Init(){
        KeineFreizeiten.title="Keine Freizeiten gefunden";
    }


    private static void loadInfos(final int werk){
        final String urlforInfo="gjwinfoexport.php?ZC=dslVp9ZRPpJlCt8sOVFx6vXerlMRTri8Kvn92KCkkNhyObA4rZc42gfGZxztGj2I";
        System.out.println("Loading Infos");
        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    String url=werkeListen[werk]+urlforInfo;
                    XmlPullParser parser=Xml.newPullParser();
                    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES,false);
                    parser.setInput(new URL(url).openStream(),"utf-8");
                    int eventType = parser.getEventType();
                    String lastType="";
                    while(eventType != XmlPullParser.END_DOCUMENT){
                        if(eventType == XmlPullParser.START_TAG){
                            lastType = parser.getName();
                        }else if(eventType == XmlPullParser.TEXT){
                            switch(lastType){
                                case "GJW_lang":
                                    info_GJW_lang=parser.getText();
                                    break;
                                case "GJW_Strasse":
                                    info_GJW_Straße=parser.getText();
                                    break;
                                case "GJW_PLZ":
                                    info_GJW_Plz=parser.getText();
                                    break;
                                case "GJW_ORT":
                                    info_GJW_Ort=parser.getText();
                                    break;
                                case "GJW_Telefon":
                                    info_GJW_Telefon=parser.getText();
                                    break;
                                case "GJW_Fax":
                                    info_GJW_Telefax=parser.getText();
                                    break;
                                case "GJW_Email":
                                    info_GJW_Email=parser.getText();
                                    break;
                                case "GJW_Website":
                                    info_GJW_Website=parser.getText();
                                    break;
                            }
                            lastType="";
                        }
                        eventType = parser.next();
                    }
                    System.out.println("Showing");
                    MainActivity.instance.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.InfoTextViews[0].setText(info_GJW_lang!=null?info_GJW_lang:"");
                            MainActivity.InfoTextViews[1].setText(info_GJW_Straße!=null?info_GJW_Straße:"");
                            MainActivity.InfoTextViews[2].setText(info_GJW_Plz!=null?info_GJW_Plz:""+" "+info_GJW_Ort!=null?info_GJW_Ort:"");
                            MainActivity.InfoTextViews[3].setText(info_GJW_Telefon!=null?"Tel:"+info_GJW_Telefon:""+ info_GJW_Telefax!=null?"Fax:"+info_GJW_Telefax:"");
                            MainActivity.InfoTextViews[4].setText(info_GJW_Email!=null?info_GJW_Email:""+" "+info_GJW_Website!=null?info_GJW_Website:"");
                        }
                    });

                }catch (final Exception e){
                    MainActivity.instance.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.instance,"Fehler beim Laden der Werk infos. Bitte versuchen sie es später nochmal",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    t.start();

    }

    public static void loadFreizeiten(final int werk, final RecyclerView recyclerView,final ProgressDialog progressDialog){
        final String urlforExport="fzlisteexport.php?ZC=tTcxCnko21Cx0lNjlqLE1OgZh3LmpEBOeCSLbBcSnoiq4cmEbMX9bW131SdN8lNE";
        Freizeiten=new ArrayList[12];

        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {

                String url="";
                    try {
                            Freizeiten[werk] = new ArrayList<Freizeit>();
                            url= werkeListen[werk]+urlforExport;

                            XmlPullParser parser = Xml.newPullParser();
                            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                            parser.setInput(new URL(url).openStream(), "utf-8");
                            int eventType = parser.getEventType();
                            String lastType = "";
                            while (eventType != XmlPullParser.END_DOCUMENT) {
                                if (eventType == XmlPullParser.START_DOCUMENT) {
                                    System.out.println("Start document");
                                } else if (eventType == XmlPullParser.START_TAG) {

                                    if (parser.getName().equals("Freizeit")) {
                                        Freizeiten[werk].add(new Freizeit());
                                        getLastFreizeit(werk).werkID=werk;
                                        System.out.println("Neue Freizeit");
                                    }
                                    lastType = parser.getName();

                                } else if (eventType == XmlPullParser.END_TAG) {

                                } else if (eventType == XmlPullParser.TEXT) {
                                    switch (lastType) {
                                        case "FZ_ID":
                                            getLastFreizeit(werk).id = parser.getText();
                                            System.out.println("ID:" + parser.getText());
                                            break;
                                        case "FZ_Ort":
                                            getLastFreizeit(werk).place = parser.getText();
                                            System.out.println("Place:" + parser.getText());
                                            break;
                                        case "FZ_Alter_min":
                                            try {
                                                getLastFreizeit(werk).minAge = Integer.parseInt(parser.getText());

                                            } catch (Exception e) {
                                                getLastFreizeit(werk).minAge = 0;
                                            }
                                                System.out.println("MinAge:" + parser.getText());
                                            break;
                                        case "FZ_Alter_max":
                                            try {
                                                getLastFreizeit(werk).maxAge = Integer.parseInt(parser.getText());

                                            } catch (Exception e) {
                                                getLastFreizeit(werk).maxAge = 99;
                                            }
                                                System.out.println("MaxAge:" + parser.getText());
                                            break;
                                        case "FZ_Beginn":
                                            getLastFreizeit(werk).begin = convertDate(parser.getText());
                                            System.out.println("Begin:" + parser.getText());
                                            break;
                                        case "FZ_Ende":
                                            getLastFreizeit(werk).end = convertDate(parser.getText());
                                            System.out.println("End:" + parser.getText());
                                            break;
                                        case "FZ_Preis":
                                            getLastFreizeit(werk).price = parser.getText();
                                            System.out.println("Preis:" + parser.getText());
                                            break;
                                        case "FZ_Titel":
                                            getLastFreizeit(werk).title = parser.getText();
                                            System.out.println("Titel:" + parser.getText());
                                        default:
                                            break;
                                    }
                                    lastType = "";
                                }
                                eventType = parser.next();
                            }
                            System.out.print("End document");

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("Error","Error loading Werk:"+FreizeitFragment.werke[werk]);
                            Log.e("Error URL",url);
                            MainActivity.instance.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recyclerView.setAdapter(null);
                                    progressDialog.dismiss();
                                    Toast.makeText(MainActivity.instance,"Fehler beim Laden. Versuchen sie es später nochmal",Toast.LENGTH_SHORT).show();

                                }
                            });
                        }


                    ArrayList<Freizeit> freizeitForMember=new ArrayList<>();
                    FreizeitAdapter freizeitAdapter;
                    Member currentMember= null;
                    int age=0;
                    if(EditFragment.memberlist.size()>0) {
                        currentMember=EditFragment.memberlist.get(FreizeitFragment.SelectedMemberIndex);
                        age=currentMember.getAge();
                        System.out.println("AGE:"+age);
                    }

                    if(currentMember!=null) {
                        for (Freizeit f : Freizeit.Freizeiten[werk]) {
                            if (age<=f.maxAge && age>= f.minAge){
                                freizeitForMember.add(f);
                            }
                        }
                         freizeitAdapter=new FreizeitAdapter(freizeitForMember);
                        if(freizeitAdapter.getItemCount()==0){
                            freizeitForMember.add(KeineFreizeiten);
                            freizeitAdapter.notifyDataSetChanged();
                        }
                    }else {
                          freizeitAdapter = new FreizeitAdapter(Freizeit.Freizeiten[werk]);
                        if(freizeitAdapter.getItemCount()==0){
                            Freizeit.Freizeiten[werk].add(KeineFreizeiten);
                            freizeitAdapter.notifyDataSetChanged();
                        }
                    }



                    final FreizeitAdapter mAdapter= freizeitAdapter;
                        MainActivity.instance.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadInfos(werk);
                        recyclerView.setAdapter(mAdapter);
                        progressDialog.dismiss();
                    }
                });
                    }


            //}
        });
        t.start();
    }

    private static String convertDate(String date){
        String year=date.substring(0,4);
        String month=date.substring(5,7);
        String day=date.substring(8,10);
        return day+"."+month+"."+year;
    }

    public static Freizeit getLastFreizeit(int werkid){
        return Freizeiten[werkid].get(Freizeiten[werkid].size()-1);
    }


}
