package de.gjw.gjwfreizeitanmeldung.ui.Members;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.gjw.gjwfreizeitanmeldung.MainActivity;
import de.gjw.gjwfreizeitanmeldung.R;
import de.gjw.gjwfreizeitanmeldung.ui.settings.SettingsFragment;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.ViewHolder> {

    private ArrayList<Member> members;

    public MemberAdapter(ArrayList<Member> members){
        this.members = members;

    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public LinearLayout layout;
        public ViewHolder(LinearLayout v){
            super(v);
            layout=v;
        }

    }

    @NonNull
    @Override
    public MemberAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout v= (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.member_list_item,parent,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final @NonNull MemberAdapter.ViewHolder holder,final int position) {
        TextView surname=holder.layout.findViewById(R.id.surname);
        TextView name=holder.layout.findViewById(R.id.name);
        TextView bday=holder.layout.findViewById(R.id.bday);
        ImageButton edit=holder.layout.findViewById(R.id.edit_btn);
        ImageButton delete= holder.layout.findViewById(R.id.delete_btn);

        surname.setText(members.get(position).getSurname());
        name.setText(members.get(position).getName());

        if(members.get(position).getBday()!=null) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
            String output = formatter.format(members.get(position).getBday()); //eg: "Tue May"
            bday.setText(output);
        }


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditDialog editDialog=new EditDialog(MainActivity.instance,members.get(position));
            }
        });


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditFragment.memberlist.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
                if(SettingsFragment.PreSelectedMemberIndex==holder.getAdapterPosition()){
                    if(SettingsFragment.PreSelectedMemberIndex>0) {
                        SettingsFragment.PreSelectedMemberIndex -= 1;
                    }else{
                        SettingsFragment.PreSelectedMemberIndex=0;
                    }
                }
                EditFragment.editinstance.itemInserted();
            }
        });

    }



    @Override
    public int getItemCount() {
        return members.size();
    }
}
