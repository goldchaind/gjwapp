package de.gjw.gjwfreizeitanmeldung.ui.settings;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import de.gjw.gjwfreizeitanmeldung.MainActivity;
import de.gjw.gjwfreizeitanmeldung.R;
import de.gjw.gjwfreizeitanmeldung.ui.Members.EditFragment;
import de.gjw.gjwfreizeitanmeldung.ui.Members.Member;
import de.gjw.gjwfreizeitanmeldung.ui.freizeit.Freizeit;
import de.gjw.gjwfreizeitanmeldung.ui.freizeit.FreizeitFragment;

public class SettingsFragment extends Fragment {

    private SettingsViewModel settingsViewModel;

    public static int PreSelectedWerkIndex=0;
    public static int PreSelectedMemberIndex=0;
    public static final String encoding="ISO-8859-1";
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        settingsViewModel =
                ViewModelProviders.of(this).get(SettingsViewModel.class);

        MainActivity.hideFab();

        View root = inflater.inflate(R.layout.fragment_settings, container, false);
        final TextView textView = root.findViewById(R.id.text_send);

        if(EditFragment.memberlist.size()>0) {

            final Spinner memberselector = root.findViewById(R.id.settings_members);

            ArrayList<String> membernames = new ArrayList<>();
            for (Member mem:EditFragment.memberlist) {
                membernames.add(mem.getName()+" "+mem.getSurname());
            }

            ArrayAdapter<String> membernamesadapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, membernames);

            memberselector.setAdapter(membernamesadapter);
            if(PreSelectedMemberIndex>=membernames.size()){
                if(membernames.size()!=0) {
                    PreSelectedMemberIndex = membernames.size() - 1;
                }else{
                    PreSelectedMemberIndex=0;
                }
                saveSettings();
            }
            memberselector.setSelection(PreSelectedMemberIndex);

            memberselector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PreSelectedMemberIndex = position;
                    FreizeitFragment.SelectedMemberIndex=position;
                   saveSettings();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }else{
            textView.setText(textView.getText()+"\nBitte fügen sie zuerst ein Familienmitglied hinzu");
        }


        final Spinner werkselector = root.findViewById(R.id.settings_werk);

        ArrayAdapter<String> werkeadapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, FreizeitFragment.werke);

        werkselector.setAdapter(werkeadapter);

        werkselector.setSelection(PreSelectedWerkIndex);

        werkselector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FreizeitFragment.SelectedWerkIndex=position;
                PreSelectedWerkIndex=position;
                saveSettings();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return root;
    }

    private static void saveSettings(){
        OutputStreamWriter settingsWriter=null;
        try{
            String path=MainActivity.instance.getFilesDir().getAbsolutePath()+"/preselected.txt";
            settingsWriter=new OutputStreamWriter(new FileOutputStream(path));
            settingsWriter.write(PreSelectedMemberIndex);
            settingsWriter.write(PreSelectedWerkIndex);
            settingsWriter.flush();
        }
        catch(Exception e){
            PreSelectedWerkIndex=0;
            PreSelectedMemberIndex=0;
            saveSettings();
            e.printStackTrace();
        }finally {
            if(settingsWriter!=null){
                try {
                    settingsWriter.close();
                }catch (Exception e){}
            }
        }
    }

    public static void loadSettings(){
        InputStreamReader settingsReader=null;
        try{
            String path=MainActivity.instance.getFilesDir().getAbsolutePath()+"/preselected.txt";
            settingsReader=new InputStreamReader(new FileInputStream(path));
            PreSelectedMemberIndex=settingsReader.read();
            PreSelectedWerkIndex=settingsReader.read();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try {
                if (settingsReader != null) {
                    settingsReader.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }

    }
}