package de.gjw.gjwfreizeitanmeldung.ui.freizeit;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.gjw.gjwfreizeitanmeldung.MainActivity;
import de.gjw.gjwfreizeitanmeldung.R;
import de.gjw.gjwfreizeitanmeldung.ui.Members.EditFragment;
import de.gjw.gjwfreizeitanmeldung.ui.Members.Member;

public class FreizeitFragment extends Fragment {

    private LoginViewModel loginViewModel;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    public static Context context;

    public static int SelectedMemberIndex;
    public static int SelectedWerkIndex;

    private static final String ladeTitle="Lade";
    private static final String ladeMessage="Lade Freizeiten";

    public final static  String[] werke=new String[]
            {
                    "GJW Deutschland",
                    "GJW Baden-Württemberg",
                    "GJW Bayern",
                    "GJW Berlin-Brandenburg",
                    "GJW Hessen",
                    "GJW Mecklenburg-Vorpommern",
                    "GJW Norddeutschland",
                    "GJW Niedersachsen Ostwestfalen",
                    "GJW Nordrhein-Westfalen",
                    "GJW Nordwestdeutschland",
                    "GJW Sachsen",
                    "GJW Thüringen"
            };



    public View onCreateView(@NonNull LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        View root = inflater.inflate(R.layout.fragment_freizeit, container, false);




        recyclerView=root.findViewById(R.id.freizeiten_liste);
        recyclerView.setHasFixedSize(true);

        layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        MainActivity.hideFab();

        final Spinner werkselector = root.findViewById(R.id.freizeit_werk);

            ArrayAdapter<String> werkeadapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, werke);

            werkselector.setAdapter(werkeadapter);

            werkselector.setSelection(SelectedWerkIndex);

            werkselector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SelectedWerkIndex=position;
                    ProgressDialog progressDialog=ProgressDialog.show(getContext(),ladeTitle,ladeMessage,true);
                    Freizeit.loadFreizeiten(position,recyclerView,progressDialog);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


        final Spinner memberselector = root.findViewById(R.id.freizeit_member);

        if(EditFragment.memberlist.size()>0) {

            ArrayList<String> membernames = new ArrayList<>();
            for (Member mem:EditFragment.memberlist) {
                membernames.add(mem.getName()+" "+mem.getSurname());
            }

            ArrayAdapter<String> membernamesadapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, membernames);

            memberselector.setAdapter(membernamesadapter);
            if(SelectedMemberIndex>=membernames.size()){
                if(membernames.size()!=0) {
                    SelectedMemberIndex = membernames.size() - 1;
                }else{
                    SelectedMemberIndex=0;
                }
            }

            memberselector.setSelection(SelectedMemberIndex);

            memberselector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    SelectedMemberIndex=position;

                    ProgressDialog progressDialog=ProgressDialog.show(getContext(),ladeTitle,ladeMessage,true);
                    Freizeit.loadFreizeiten(SelectedWerkIndex,recyclerView,progressDialog);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    SelectedMemberIndex=0;
                    ProgressDialog progressDialog=ProgressDialog.show(getContext(),ladeTitle,ladeMessage,true);
                    Freizeit.loadFreizeiten(SelectedWerkIndex,recyclerView,progressDialog);

                }
            });
        }else{
            ArrayAdapter<String> membernamesadapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item,new ArrayList<String>());
            membernamesadapter.add("Bitte fügen sie Familienmitglieder hinzu");
            memberselector.setAdapter(membernamesadapter);
        }


        return root;
    }



}