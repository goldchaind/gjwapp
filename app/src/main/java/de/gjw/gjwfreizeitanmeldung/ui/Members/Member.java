package de.gjw.gjwfreizeitanmeldung.ui.Members;

import java.io.Serializable;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

public class Member implements Serializable {

    String name;
    String surname;
    Date bday;
    int age;
    int geschlecht;

    String street;
    String number;
    String letter;
    String zusatz;
    String post;
    String place;

    String email;
    String Tvor,Tnum;
    String Hvor,Hnum;



    public int getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(int geschlecht) {
        this.geschlecht = geschlecht;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public String getZusatz() {
        return zusatz;
    }

    public void setZusatz(String zusatz) {
        this.zusatz = zusatz;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTvor() {
        return Tvor;
    }

    public void setTvor(String tvor) {
        Tvor = tvor;
    }

    public String getTnum() {
        return Tnum;
    }

    public void setTnum(String tnum) {
        Tnum = tnum;
    }

    public String getHvor() {
        return Hvor;
    }

    public void setHvor(String hvor) {
        Hvor = hvor;
    }

    public String getHnum() {
        return Hnum;
    }

    public void setHnum(String hnum) {
        Hnum = hnum;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Date getBday() {
        return bday;
    }

    public void setBday(Date bday) {
        this.bday = bday;
    }

    public int getAge() {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(getBday());
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }
        Integer ageInt = new Integer(age);
        return ageInt;


    }

}
