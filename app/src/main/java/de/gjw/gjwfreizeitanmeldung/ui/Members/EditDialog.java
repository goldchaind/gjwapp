package de.gjw.gjwfreizeitanmeldung.ui.Members;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.gjw.gjwfreizeitanmeldung.MainActivity;
import de.gjw.gjwfreizeitanmeldung.R;

public class EditDialog  {

    //Member member;
    AlertDialog dialog;
    final Member member;

    public EditDialog(Activity activity,final Member memberp){
        if(memberp==null){
            member=new Member();
        }else{
            member=memberp;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();

        View layout=inflater.inflate(R.layout.edit_dialog,null);
        final EditText surname=layout.findViewById(R.id.surname_edit);
        final EditText name=layout.findViewById(R.id.name_edit);
        final EditText bday=layout.findViewById(R.id.bday_edit);

        bday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                final int day = c.get(Calendar.DAY_OF_MONTH);

                // Create a new instance of DatePickerDialog and return it
                DatePickerDialog dateSelector=new DatePickerDialog(MainActivity.instance, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, month, dayOfMonth);

                        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
                        String output = formatter.format(calendar.getTime()); //eg: "Tue May"
                        bday.setText(output);
                        member.setBday(calendar.getTime());
                    }
                }, year, month, day);

                dateSelector.show();
            }
        });





        final Spinner geschlecht=layout.findViewById(R.id.geschlecht_edit);

        final EditText street=layout.findViewById(R.id.adress_street_edit);
        final EditText number=layout.findViewById(R.id.adress_number_edit);
        final EditText letter=layout.findViewById(R.id.adress_letter_edit);
        final EditText zusatz=layout.findViewById(R.id.adress_zusatz_edit);
        final EditText post=layout.findViewById(R.id.post_edit);
        final EditText place=layout.findViewById(R.id.place_edit);

        final EditText email=layout.findViewById(R.id.email_edit);
        final EditText Tvor=layout.findViewById(R.id.tel_vor_edit);
        final EditText Tnum=layout.findViewById(R.id.tel_num_edit);
        final EditText Hvor=layout.findViewById(R.id.han_vor_edit);
        final EditText Hnum=layout.findViewById(R.id.han_num_edit);



        surname.setText(member.getSurname());
        name.setText(member.getName());

        if(member.getBday()!=null) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
            String output = formatter.format(member.getBday()); //eg: "Tue May"
            bday.setText(output);
        }

        geschlecht.setSelection(member.getGeschlecht());

        street.setText(member.getStreet());
        number.setText(member.getNumber());
        letter.setText(member.getLetter());
        zusatz.setText(member.getZusatz());
        post.setText(member.getPost());
        place.setText(member.getPlace());

        email.setText(member.getEmail());
        Tvor.setText(member.getTvor());
        Tnum.setText(member.getTnum());
        Hvor.setText(member.getHvor());
        Hnum.setText(member.getHnum());

        builder.setView(layout)
                .setPositiveButton("Speichern", null)
                .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        final AlertDialog dialog=builder.create();
        dialog.show();

        Button positive=dialog.getButton(Dialog.BUTTON_POSITIVE);
        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean error=false;
                if(TextUtils.isEmpty(name.getText())){
                    error=true;
                    name.setError("Feld erforderlich");
                }else{
                    name.setError(null);
                }
                if(TextUtils.isEmpty(surname.getText())){
                    error=true;
                    surname.setError("Feld erforderlich");
                }else{
                    surname.setError(null);
                }
                if(TextUtils.isEmpty(bday.getText())){
                    error=true;
                    bday.setError("Feld erforderlich");
                }else{
                    bday.setError(null);
                }

                if(TextUtils.isEmpty(street.getText())){
                    error=true;
                    street.setError("Feld erforderlich");
                }else{
                    street.setError(null);
                }
                if(TextUtils.isEmpty(number.getText())){
                    error=true;
                    number.setError("Feld erforderlich");
                }else{
                    number.setError(null);
                }
                //Letter & Zusatz
                if(TextUtils.isEmpty(post.getText())){
                    error=true;
                    post.setError("Feld erforderlich");
                }else{
                    post.setError(null);
                }
                if(TextUtils.isEmpty(place.getText())){
                    error=true;
                    place.setError("Feld erforderlich");
                }else{
                    place.setError(null);
                }
                if(TextUtils.isEmpty(email.getText())){
                    error=true;
                    email.setError("Feld erforderlich");
                }else{
                    email.setError(null);
                }

                if(!error) {
                    member.setSurname(surname.getText().toString());
                    member.setName(name.getText().toString());
                    //member.setBday(bday.getText().toString());
                    member.setGeschlecht(geschlecht.getSelectedItemPosition());

                    member.setStreet(street.getText().toString());
                    member.setNumber(number.getText().toString());
                    member.setLetter(letter.getText().toString());
                    member.setZusatz(zusatz.getText().toString());
                    member.setPost(post.getText().toString());
                    member.setPlace(place.getText().toString());

                    member.setEmail(email.getText().toString());
                    member.setTvor(Tvor.getText().toString());
                    member.setTnum(Tnum.getText().toString());
                    member.setHnum(Hnum.getText().toString());
                    member.setHvor(Hvor.getText().toString());

                    if (memberp == null) {
                        EditFragment.memberlist.add(member);
                    }


                    EditFragment.editinstance.itemInserted();
                    dialog.dismiss();
                }

            }
        });
    }
}