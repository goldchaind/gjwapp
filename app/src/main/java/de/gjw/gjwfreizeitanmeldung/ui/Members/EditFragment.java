package de.gjw.gjwfreizeitanmeldung.ui.Members;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import de.gjw.gjwfreizeitanmeldung.MainActivity;
import de.gjw.gjwfreizeitanmeldung.R;
import de.gjw.gjwfreizeitanmeldung.ui.settings.SettingsFragment;

public class EditFragment extends Fragment {

    private EditViewModel editViewModel;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    public static EditFragment editinstance;

    public static ArrayList<Member> memberlist=new ArrayList<Member>();


    public View onCreateView(@NonNull LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        editinstance=this;
        editViewModel = ViewModelProviders.of(this).get(EditViewModel.class);
        View root = inflater.inflate(R.layout.fragment_members, container, false);

        MainActivity.showFab();

        recyclerView=root.findViewById(R.id.memberlist);
        recyclerView.setHasFixedSize(true);

        layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new MemberAdapter(memberlist);
        recyclerView.setAdapter(mAdapter);

        MainActivity.getFab().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditDialog add = new EditDialog(getActivity(),null);
            }
        });

        return root;
    }
    //public static Integer PreSelectedIndex=0;

    private static void saveMembers(){
        FileOutputStream fout=null;
        ObjectOutputStream oos=null;

        try {
            String path=MainActivity.instance.getFilesDir().getAbsolutePath()+"/memberlist.txt";
            fout = new FileOutputStream(path);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(memberlist);

        }catch(IOException e){
            e.printStackTrace();
        }finally{
            try {
                if (fout != null) {
                    fout.close();
                }
                if (oos != null) {
                    oos.close();
                }
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    private static boolean initialloaded=false;

    public static void loadMembers(){
        if(initialloaded==false) {
            FileInputStream fin = null;
            ObjectInputStream ois = null;
            FileInputStream finP = null;
            ObjectInputStream oisP = null;

            try{
                String path = MainActivity.instance.getFilesDir().getAbsolutePath() + "/memberlist.txt";
                fin = new FileInputStream(path);
                ois = new ObjectInputStream(fin);
                memberlist = (ArrayList<Member>) ois.readObject();
            }catch(Exception e){
                e.printStackTrace();
            }
            finally {
                try {
                    if(fin!=null){
                        fin.close();
                    }
                    if(ois!=null){
                        ois.close();
                    }
                }catch(IOException e){
                    e.printStackTrace();
                }
            }


        }
    }

    public void itemInserted(){
        mAdapter.notifyDataSetChanged();
        saveMembers();
    }


}