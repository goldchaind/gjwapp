package de.gjw.gjwfreizeitanmeldung.ui.freizeit;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;


import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.gjw.gjwfreizeitanmeldung.MainActivity;
import de.gjw.gjwfreizeitanmeldung.R;
import de.gjw.gjwfreizeitanmeldung.ui.Members.EditDialog;
import de.gjw.gjwfreizeitanmeldung.ui.Members.EditFragment;
import de.gjw.gjwfreizeitanmeldung.ui.Members.Member;
import de.gjw.gjwfreizeitanmeldung.ui.freizeit.Freizeit;
import de.gjw.gjwfreizeitanmeldung.ui.settings.SettingsFragment;

public class FreizeitAdapter extends RecyclerView.Adapter<FreizeitAdapter.ViewHolder> {


    public final static String[] werkeAnmeldungURLS=new String[]
            {
                    "https://www.gjw.de/anmeldung/#",
                    "https://www.gjw-bawue.de/anmeldung/#",
                    "https://www.gjw-bayern.de/anmeldung/#",
                    "https://www.gjw-bb.de/anmeldung/#",
                    "http://www.gjw-hessen.de/anmeldung/#",
                    "http://www.gjw-mv.de/anmeldung/#",
                    "https://www.gjw-nd.de/anmeldung/#",
                    "https://www.gjw-nos.de/anmeldung/#",
                    "https://www.gjw-nrw.de/anmeldung/#",
                    "https://www.gjw-nwd.de/anmeldung/#",
                    "https://www.gjw-sachsen.de/anmeldung/#",
                    "https://www.gjw-thüringen.de/anmeldung/#"
            };

    private ArrayList<Freizeit> freizeiten;

    public FreizeitAdapter(ArrayList<Freizeit> members){
        this.freizeiten = members;

    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ConstraintLayout layout;
        public ViewHolder(ConstraintLayout v){
            super(v);
            layout=v;
        }

    }

    @NonNull
    @Override
    public FreizeitAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout v= (ConstraintLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.freizeit_list_item,parent,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final @NonNull FreizeitAdapter.ViewHolder holder,final int position) {
        TextView title=holder.layout.findViewById(R.id.freizeit_title);
        TextView date=holder.layout.findViewById(R.id.freizeit_date);
        TextView place=holder.layout.findViewById(R.id.freizeit_place);
        TextView price=holder.layout.findViewById(R.id.freizeit_price);
        ImageButton login=holder.layout.findViewById(R.id.login);
        final Freizeit currentFreizeit=freizeiten.get(position);


        title.setText(currentFreizeit.title);
        if(currentFreizeit!=Freizeit.KeineFreizeiten) {
            date.setText(currentFreizeit.begin + " - " + currentFreizeit.end);
            place.setText(currentFreizeit.place);
            price.setText(currentFreizeit.price + "€");

            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Member currentMember;
                    if (EditFragment.memberlist.size() > FreizeitFragment.SelectedMemberIndex && FreizeitFragment.SelectedMemberIndex >= 0) {
                        currentMember = EditFragment.memberlist.get(FreizeitFragment.SelectedMemberIndex);
                    } else {
                        currentMember = null;
                    }

                    if (currentMember != null) {
                        String LoginUrl = werkeAnmeldungURLS[currentFreizeit.werkID];
                        try {
                            LoginUrl += "" + currentFreizeit.id;
                            LoginUrl += "&qi=APP-Anmeldung";
                            String encoding = SettingsFragment.encoding;
                            if (currentMember.getSurname().isEmpty() == false) {
                                LoginUrl += "&PNN=" + URLEncoder.encode(currentMember.getSurname(), encoding);
                            }
                            if (currentMember.getName().isEmpty() == false) {
                                LoginUrl += "&PVN=" + URLEncoder.encode(currentMember.getName(), encoding);
                            }
                            if (currentMember.getBday() != null) {
                                LoginUrl += "&PGT=" + URLEncoder.encode(formateDate(currentMember.getBday()), encoding);
                            }
                            if (currentMember.getGeschlecht() != -1) {
                                LoginUrl += "&PGS=" + URLEncoder.encode(String.valueOf((currentMember.getGeschlecht() + 1) * 10));
                            }
                            if (currentMember.getStreet().isEmpty() == false) {
                                LoginUrl += "&AST=" + URLEncoder.encode(currentMember.getStreet(), encoding);
                            }
                            if (currentMember.getNumber().isEmpty() == false) {
                                LoginUrl += "&ANR=" + URLEncoder.encode(currentMember.getNumber(), encoding);
                            }
                            if (currentMember.getLetter().isEmpty() == false) {
                                LoginUrl += "&ABS=" + URLEncoder.encode(currentMember.getLetter(), encoding);
                            }
                            if (currentMember.getZusatz().isEmpty() == false) {
                                LoginUrl += "&AZS=" + URLEncoder.encode(currentMember.getZusatz(), encoding);
                            }
                            if (currentMember.getPost().isEmpty() == false) {
                                LoginUrl += "&APL=" + URLEncoder.encode(currentMember.getPost(), encoding);
                            }
                            if (currentMember.getPlace().isEmpty() == false) {
                                LoginUrl += "&AOR=" + URLEncoder.encode(currentMember.getPlace(), encoding);
                            }
                            if (currentMember.getEmail().isEmpty() == false) {
                                LoginUrl += "&EMA=" + URLEncoder.encode(currentMember.getEmail(), encoding);
                            }
                            if (currentMember.getTvor().isEmpty() == false) {
                                LoginUrl += "&TVW=" + URLEncoder.encode(currentMember.getTvor(), encoding);
                            }
                            if (currentMember.getTnum().isEmpty() == false) {
                                LoginUrl += "&TRN=" + URLEncoder.encode(currentMember.getTnum(), encoding);
                            }
                            if (currentMember.getHvor().isEmpty() == false) {
                                LoginUrl += "&HVW=" + URLEncoder.encode(currentMember.getHvor(), encoding);
                            }
                            if (currentMember.getHnum().isEmpty() == false) {
                                LoginUrl += "&HRN=" + URLEncoder.encode(currentMember.getHnum(), encoding);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(LoginUrl));
                            MainActivity.instance.startActivity(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.instance, "Fehler beim öffnen des Browsers", Toast.LENGTH_SHORT).show();
                        }
                        System.out.println(Uri.parse(LoginUrl));

                    }
                }
            });
        }else{
            date.setVisibility(View.INVISIBLE);
            place.setVisibility(View.INVISIBLE);
            price.setVisibility(View.INVISIBLE);
            login.setVisibility(View.INVISIBLE);
        }



    }

    private String formateDate(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("YYYYMMdd");//PHP Format
        String output = formatter.format(date); //eg: "Tue May"
        return output;
    }





    @Override
    public int getItemCount() {
        return freizeiten.size();
    }
}
