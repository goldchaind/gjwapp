package de.gjw.gjwfreizeitanmeldung;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.Settings;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import de.gjw.gjwfreizeitanmeldung.ui.Members.EditFragment;
import de.gjw.gjwfreizeitanmeldung.ui.freizeit.Freizeit;
import de.gjw.gjwfreizeitanmeldung.ui.freizeit.FreizeitFragment;
import de.gjw.gjwfreizeitanmeldung.ui.settings.SettingsFragment;

public class MainActivity extends AppCompatActivity {

    public static TextView InfoTextViews[]=new TextView[5];
    private AppBarConfiguration mAppBarConfiguration;

    static FloatingActionButton fab;
    public static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        instance=this;

        try {
            EditFragment.loadMembers();
            SettingsFragment.loadSettings();
            FreizeitFragment.SelectedMemberIndex= SettingsFragment.PreSelectedMemberIndex;
            FreizeitFragment.SelectedWerkIndex= SettingsFragment.PreSelectedWerkIndex;
            Freizeit.Init();
        }catch(Exception e){
            AlertDialog.Builder ab=new AlertDialog.Builder(this);
            ab.setMessage("Leider ist ein Fehler beim Laden der Daten aufgetreten, weshalb evtl. ein Datenverlust möglich ist. Das betrifft lediglich auf dem Gerät gespeicherte Familienmitglieder");
            ab.setTitle("Fehler");
            ab.setPositiveButton("Ok",null);
            ab.show().show();


        }


        fab = findViewById(R.id.fab);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View navigationHeader = navigationView.getHeaderView(0);

        InfoTextViews[0]=navigationHeader.findViewById(R.id.info_name);
        InfoTextViews[1]=navigationHeader.findViewById(R.id.info_adresse);
        InfoTextViews[2]=navigationHeader.findViewById(R.id.info_plz);
        InfoTextViews[3]=navigationHeader.findViewById(R.id.info_tel);
        InfoTextViews[4]=navigationHeader.findViewById(R.id.info_web);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_login, R.id.nav_edit, R.id.nav_settings)
                .setDrawerLayout(drawer)
                .build();




        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        if(EditFragment.memberlist.size()==0) {
            drawer.openDrawer(Gravity.LEFT);
        }
    }

    public static void showFab(){
        fab.show();
    }

    public static void hideFab(){
        fab.hide();
    }

    public static FloatingActionButton getFab(){
        return fab;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
